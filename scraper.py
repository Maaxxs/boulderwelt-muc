import requests
from bs4 import BeautifulSoup
import re
import sqlite3
from datetime import datetime
from time import sleep
import os


def get_values():
    website = requests.get("https://www.boulderwelt-muenchen-ost.de/")

    soup = BeautifulSoup(website.text, "html.parser")
    people_waiting_div = soup.find("div", attrs={"class": "corona-max"})
    people_waiting = re.search(r"\d\d?\d?", people_waiting_div.text).group()
    # print(people_waiting)

    div_capacity = soup.find(
        "div", attrs={"class": "crowd-level-tag crowd-level-pointer"}
    )
    capacity = div_capacity.find("div").text.rstrip("%")
    # print(capacity)

    date_time = datetime.now().strftime("%Y-%m-%d %H:%M")

    print(
        f"DateTime: {date_time}  Capacity: {capacity:3}  People waiting: {people_waiting}"
    )

    write_to_db(date_time, capacity, people_waiting)


def write_to_db(date_time: str, capacity: int, people: int):
    conn = sqlite3.connect("boulderhalle.db")
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO boulder_muc VALUES (?, ?, ?)", (date_time, capacity, people)
    )
    conn.commit()
    conn.close()


def main():
    conn = sqlite3.connect("boulderhalle.db")
    cur = conn.cursor()
    cur.execute(
        "CREATE TABLE IF NOT EXISTS boulder_muc (date_time TEXT, capacity INTEGER, people INTEGER)"
    )
    conn.commit()
    conn.close()

    while True:
        get_values()
        sleep(300)


if __name__ == "__main__":
    main()
