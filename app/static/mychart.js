function getAll() {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.onload = (event) => resolve(JSON.parse(request.response));
    request.onerror = (event) => reject(event);
    request.open("GET", "/api/v1/all");
    request.send();
  });
}

async function build_graph() {
  const data = await getAll();

  const ctx = document.getElementById("myChart").getContext("2d");
  const myChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: data["date_time"],
      datasets: [
        {
          label: "Capacity in %",
          data: data["capacity"],
          fill: false,
          backgroundColor: ["rgba(54, 162, 235, 0.2)"],
          borderColor: ["rgba(54, 162, 235, 1)"],
          borderWidth: 1,
          yAxisID: "capacity-axis",
        },
        {
          label: "Waiting people",
          fill: false,
          data: data["people"],
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          borderColor: "rgba(255, 99, 132, 1)",
          borderWidth: 1,
          yAxisID: "people-axis",
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: "Boulderhalle München Ost",
      },
      scales: {
        yAxes: [
          {
            type: "linear",
            display: "true",
            postition: "left",
            id: "capacity-axis",
            ticks: {
              beginAtZero: true,
              suggestedMin: 0,
              suggestedMax: 100,
            },
          },
          {
            type: "linear",
            display: "true",
            position: "right",
            id: "people-axis",
            ticks: {
              beginAtZero: true,
              stepSize: 1,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  });
}

window.onload = () => {
  build_graph()
  setInterval(build_graph, 300000);
}
