from flask import Flask, render_template, jsonify
import sqlite3

# import jsonify


app = Flask(__name__)
app.config["DEBUG"] = True


@app.route("/", methods=["GET"])
@app.route("/index.html", methods=["GET"])
def index():
    return render_template("index.html")


@app.route("/api/v1/all", methods=["GET"])
def api_all():
    with sqlite3.connect("boulderhalle.db") as conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM boulder_muc ORDER BY datetime(date_time)")
        data = cur.fetchall()
        dic = {}
        dic["date_time"] = [tup[0] for tup in data]
        dic["capacity"] = [tup[1] for tup in data]
        dic["people"] = [tup[2] for tup in data]
        # print(dic)
        return jsonify(dic)


if __name__ == "__main__":
    app.run(port=5000)
